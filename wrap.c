/*

  This file is part of TALER
  Copyright (C) 2014, 2015 GNUnet e.V.

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
*/


#include <stdio.h>
#include <gnunet/platform.h>
#include <gnunet/gnunet_config.h>
#include <gnunet/gnunet_util_taler_wallet_lib.h>
#include <taler/taler_util_wallet.h>
#include <taler/taler_signatures.h>


/**
 * Allocate and return a public EDDSA key
 *
 * @param priv the private key to which associate the generated public key
 * @return the public key
 */
struct GNUNET_CRYPTO_EddsaPublicKey *
TALER_WRALL_eddsa_public_key_from_private (const struct GNUNET_CRYPTO_EddsaPrivateKey *priv)
{
  struct GNUNET_CRYPTO_EddsaPublicKey *ptr;

  ptr = GNUNET_new (struct GNUNET_CRYPTO_EddsaPublicKey);
  GNUNET_CRYPTO_eddsa_key_get_public (priv, ptr);
  return ptr;
}



/**
 * Allocate and return a public ECDSA key
 *
 * @param priv the private key to which associate the generated public key
 * @return the public key
 */
struct GNUNET_CRYPTO_EcdsaPublicKey *
TALER_WRALL_ecdsa_public_key_from_private (const struct GNUNET_CRYPTO_EcdsaPrivateKey *priv)
{
  struct GNUNET_CRYPTO_EcdsaPublicKey *ptr;

  ptr = GNUNET_new (struct GNUNET_CRYPTO_EcdsaPublicKey);
  GNUNET_CRYPTO_ecdsa_key_get_public (priv, ptr);
  return ptr;
}


/**
 * Allocate and return a public ECDSA key
 *
 * @param priv the private key to which associate the generated public key
 * @return the public key
 */
struct GNUNET_CRYPTO_EcdhePublicKey *
TALER_WRALL_ecdhe_public_key_from_private (const struct GNUNET_CRYPTO_EcdhePrivateKey *priv)
{
  struct GNUNET_CRYPTO_EcdhePublicKey *ptr;

  ptr = GNUNET_new (struct GNUNET_CRYPTO_EcdhePublicKey);
  GNUNET_CRYPTO_ecdhe_key_get_public (priv, ptr);
  return ptr;
}


struct GNUNET_CRYPTO_EccSignaturePurpose *
TALER_WRALL_purpose_create (uint32_t purpose,
                            const void *payload,
                            size_t payload_size)
{
  struct GNUNET_CRYPTO_EccSignaturePurpose *p;
  size_t size = sizeof (struct GNUNET_CRYPTO_EccSignaturePurpose) + payload_size;

  p = GNUNET_malloc (size);
  memcpy (&p[1], payload, payload_size);
  p->purpose = htonl (purpose);
  p->size = htonl (size);

  return p;
}


/**
 * Convert bare value/fraction/cur values into a proper
 * Taler-compatible amount structure. The fractional part
 * must be already ported to the 1000000. See specifications.
 *
 * @param value value
 * @param fraction fraction
 * @param cur 0-terminated string representin the wanted currency
 * @return the pointer to freshly allocated structure
 */
struct TALER_Amount *
TALER_WRALL_get_amount (uint64_t value, uint32_t fraction, const char *cur)
{

  struct TALER_Amount amount;
  struct TALER_Amount *ptr;

  if (GNUNET_OK != TALER_amount_get_zero (cur, &amount))
    return NULL;
  amount.value = value;
  amount.fraction = fraction;
  ptr = GNUNET_new (struct TALER_Amount);
  memcpy (ptr, &amount, sizeof (amount));
  return ptr;

}


/**
 * Return the given amount's value. Useful after
 * the wallet does some arithmetic involving amounts
 * by using an emscripted function, and wants to
 * read back in JavaSript land the result of that
 * operation.
 *
 * @param a the amount to get data from
 * @return the 'value' field of this amount
 */
uint64_t
TALER_WR_get_value (const struct TALER_Amount *a)
{
  return a->value;
}


/**
 * Return the given amount's fraction. To be used as
 * its 'get_value' counterpart.
 *
 * @param a the amount to get data from
 * @return the 'fraction' field of this amount
 */
uint32_t
TALER_WR_get_fraction (const struct TALER_Amount *a)
{
  return a->fraction;
}


/**
 * Return the given amount's currency. Implemented
 * for completeness.
 *
 * @param a the amount to get data from
 * @return the 'currency' field of this amount
 */
const char *
TALER_WR_get_currency (const struct TALER_Amount *a)
{
  return a->currency;
}
