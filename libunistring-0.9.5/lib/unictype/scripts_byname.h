/* ANSI-C code produced by gperf version 3.0.4 */
/* Command-line: gperf -m 10 ./unictype/scripts_byname.gperf  */
/* Computed positions: -k'1,3,5,8' */

#if !((' ' == 32) && ('!' == 33) && ('"' == 34) && ('#' == 35) \
      && ('%' == 37) && ('&' == 38) && ('\'' == 39) && ('(' == 40) \
      && (')' == 41) && ('*' == 42) && ('+' == 43) && (',' == 44) \
      && ('-' == 45) && ('.' == 46) && ('/' == 47) && ('0' == 48) \
      && ('1' == 49) && ('2' == 50) && ('3' == 51) && ('4' == 52) \
      && ('5' == 53) && ('6' == 54) && ('7' == 55) && ('8' == 56) \
      && ('9' == 57) && (':' == 58) && (';' == 59) && ('<' == 60) \
      && ('=' == 61) && ('>' == 62) && ('?' == 63) && ('A' == 65) \
      && ('B' == 66) && ('C' == 67) && ('D' == 68) && ('E' == 69) \
      && ('F' == 70) && ('G' == 71) && ('H' == 72) && ('I' == 73) \
      && ('J' == 74) && ('K' == 75) && ('L' == 76) && ('M' == 77) \
      && ('N' == 78) && ('O' == 79) && ('P' == 80) && ('Q' == 81) \
      && ('R' == 82) && ('S' == 83) && ('T' == 84) && ('U' == 85) \
      && ('V' == 86) && ('W' == 87) && ('X' == 88) && ('Y' == 89) \
      && ('Z' == 90) && ('[' == 91) && ('\\' == 92) && (']' == 93) \
      && ('^' == 94) && ('_' == 95) && ('a' == 97) && ('b' == 98) \
      && ('c' == 99) && ('d' == 100) && ('e' == 101) && ('f' == 102) \
      && ('g' == 103) && ('h' == 104) && ('i' == 105) && ('j' == 106) \
      && ('k' == 107) && ('l' == 108) && ('m' == 109) && ('n' == 110) \
      && ('o' == 111) && ('p' == 112) && ('q' == 113) && ('r' == 114) \
      && ('s' == 115) && ('t' == 116) && ('u' == 117) && ('v' == 118) \
      && ('w' == 119) && ('x' == 120) && ('y' == 121) && ('z' == 122) \
      && ('{' == 123) && ('|' == 124) && ('}' == 125) && ('~' == 126))
/* The character set is not based on ISO-646.  */
#error "gperf generated tables don't work with this execution character set. Please report a bug to <bug-gnu-gperf@gnu.org>."
#endif

#line 4 "./unictype/scripts_byname.gperf"
struct named_script { int name; unsigned int index; };

#define TOTAL_KEYWORDS 125
#define MIN_WORD_LENGTH 2
#define MAX_WORD_LENGTH 22
#define MIN_HASH_VALUE 3
#define MAX_HASH_VALUE 171
/* maximum key range = 169, duplicates = 0 */

#ifdef __GNUC__
__inline
#else
#ifdef __cplusplus
inline
#endif
#endif
static unsigned int
scripts_hash (register const char *str, register unsigned int len)
{
  static const unsigned char asso_values[] =
    {
      172, 172, 172, 172, 172, 172, 172, 172, 172, 172,
      172, 172, 172, 172, 172, 172, 172, 172, 172, 172,
      172, 172, 172, 172, 172, 172, 172, 172, 172, 172,
      172, 172, 172, 172, 172, 172, 172, 172, 172, 172,
      172, 172, 172, 172, 172, 172, 172, 172, 172, 172,
      172, 172, 172, 172, 172, 172, 172, 172, 172, 172,
      172, 172, 172, 172, 172, 104,   7,   3,  10,   7,
      172,  35,  83,  11,  14,  47,  25,   1,  44,  50,
       31, 172,  74,   9,  15,  27,  88,  15, 172,   1,
      172, 172, 172, 172, 172,  38, 172,   4,  50,  44,
       20,  11,  26,  11,  36,   7,  66,  38,  14,   3,
        1,   2,  53, 172,   9,  59,  12,  21,   6,   2,
      172,  67, 172, 172, 172, 172, 172, 172, 172, 172,
      172, 172, 172, 172, 172, 172, 172, 172, 172, 172,
      172, 172, 172, 172, 172, 172, 172, 172, 172, 172,
      172, 172, 172, 172, 172, 172, 172, 172, 172, 172,
      172, 172, 172, 172, 172, 172, 172, 172, 172, 172,
      172, 172, 172, 172, 172, 172, 172, 172, 172, 172,
      172, 172, 172, 172, 172, 172, 172, 172, 172, 172,
      172, 172, 172, 172, 172, 172, 172, 172, 172, 172,
      172, 172, 172, 172, 172, 172, 172, 172, 172, 172,
      172, 172, 172, 172, 172, 172, 172, 172, 172, 172,
      172, 172, 172, 172, 172, 172, 172, 172, 172, 172,
      172, 172, 172, 172, 172, 172, 172, 172, 172, 172,
      172, 172, 172, 172, 172, 172, 172, 172, 172, 172,
      172, 172, 172, 172, 172, 172
    };
  register int hval = len;

  switch (hval)
    {
      default:
        hval += asso_values[(unsigned char)str[7]];
      /*FALLTHROUGH*/
      case 7:
      case 6:
      case 5:
        hval += asso_values[(unsigned char)str[4]];
      /*FALLTHROUGH*/
      case 4:
      case 3:
        hval += asso_values[(unsigned char)str[2]];
      /*FALLTHROUGH*/
      case 2:
      case 1:
        hval += asso_values[(unsigned char)str[0]];
        break;
    }
  return hval;
}

struct script_stringpool_t
  {
    char script_stringpool_str3[sizeof("Yi")];
    char script_stringpool_str6[sizeof("Mro")];
    char script_stringpool_str9[sizeof("Miao")];
    char script_stringpool_str11[sizeof("Cham")];
    char script_stringpool_str13[sizeof("Mandaic")];
    char script_stringpool_str14[sizeof("Common")];
    char script_stringpool_str15[sizeof("Myanmar")];
    char script_stringpool_str16[sizeof("Chakma")];
    char script_stringpool_str17[sizeof("Mongolian")];
    char script_stringpool_str18[sizeof("Bamum")];
    char script_stringpool_str19[sizeof("Bengali")];
    char script_stringpool_str20[sizeof("Brahmi")];
    char script_stringpool_str21[sizeof("Sinhala")];
    char script_stringpool_str22[sizeof("Carian")];
    char script_stringpool_str23[sizeof("Thai")];
    char script_stringpool_str24[sizeof("Sharada")];
    char script_stringpool_str25[sizeof("Modi")];
    char script_stringpool_str26[sizeof("Thaana")];
    char script_stringpool_str27[sizeof("Shavian")];
    char script_stringpool_str28[sizeof("Syriac")];
    char script_stringpool_str29[sizeof("Cuneiform")];
    char script_stringpool_str30[sizeof("Lao")];
    char script_stringpool_str31[sizeof("Devanagari")];
    char script_stringpool_str32[sizeof("Braille")];
    char script_stringpool_str33[sizeof("Mende_Kikakui")];
    char script_stringpool_str34[sizeof("Samaritan")];
    char script_stringpool_str35[sizeof("Cherokee")];
    char script_stringpool_str36[sizeof("Meetei_Mayek")];
    char script_stringpool_str37[sizeof("Tamil")];
    char script_stringpool_str38[sizeof("Buginese")];
    char script_stringpool_str39[sizeof("Warang_Citi")];
    char script_stringpool_str40[sizeof("Javanese")];
    char script_stringpool_str41[sizeof("Balinese")];
    char script_stringpool_str42[sizeof("Tagbanwa")];
    char script_stringpool_str43[sizeof("Latin")];
    char script_stringpool_str44[sizeof("Canadian_Aboriginal")];
    char script_stringpool_str45[sizeof("Linear_B")];
    char script_stringpool_str46[sizeof("Telugu")];
    char script_stringpool_str47[sizeof("Tagalog")];
    char script_stringpool_str48[sizeof("Tai_Tham")];
    char script_stringpool_str49[sizeof("Nko")];
    char script_stringpool_str50[sizeof("Caucasian_Albanian")];
    char script_stringpool_str51[sizeof("Phoenician")];
    char script_stringpool_str52[sizeof("Tirhuta")];
    char script_stringpool_str53[sizeof("Tai_Le")];
    char script_stringpool_str54[sizeof("Limbu")];
    char script_stringpool_str55[sizeof("Lydian")];
    char script_stringpool_str56[sizeof("Saurashtra")];
    char script_stringpool_str57[sizeof("Georgian")];
    char script_stringpool_str58[sizeof("Grantha")];
    char script_stringpool_str59[sizeof("Kannada")];
    char script_stringpool_str60[sizeof("Gothic")];
    char script_stringpool_str61[sizeof("Osmanya")];
    char script_stringpool_str62[sizeof("Batak")];
    char script_stringpool_str63[sizeof("Glagolitic")];
    char script_stringpool_str64[sizeof("Khmer")];
    char script_stringpool_str65[sizeof("Takri")];
    char script_stringpool_str66[sizeof("Oriya")];
    char script_stringpool_str67[sizeof("Manichaean")];
    char script_stringpool_str68[sizeof("Buhid")];
    char script_stringpool_str69[sizeof("Coptic")];
    char script_stringpool_str70[sizeof("Cypriot")];
    char script_stringpool_str71[sizeof("Sora_Sompeng")];
    char script_stringpool_str72[sizeof("Siddham")];
    char script_stringpool_str73[sizeof("Bopomofo")];
    char script_stringpool_str74[sizeof("Duployan")];
    char script_stringpool_str75[sizeof("Kharoshthi")];
    char script_stringpool_str76[sizeof("Inherited")];
    char script_stringpool_str77[sizeof("Meroitic_Cursive")];
    char script_stringpool_str78[sizeof("Cyrillic")];
    char script_stringpool_str79[sizeof("Lycian")];
    char script_stringpool_str80[sizeof("Gurmukhi")];
    char script_stringpool_str81[sizeof("Meroitic_Hieroglyphs")];
    char script_stringpool_str82[sizeof("Sundanese")];
    char script_stringpool_str83[sizeof("Bassa_Vah")];
    char script_stringpool_str84[sizeof("Tibetan")];
    char script_stringpool_str85[sizeof("Deseret")];
    char script_stringpool_str86[sizeof("Tifinagh")];
    char script_stringpool_str87[sizeof("Han")];
    char script_stringpool_str88[sizeof("Lisu")];
    char script_stringpool_str89[sizeof("Greek")];
    char script_stringpool_str90[sizeof("Ugaritic")];
    char script_stringpool_str91[sizeof("Syloti_Nagri")];
    char script_stringpool_str92[sizeof("Hanunoo")];
    char script_stringpool_str93[sizeof("Khojki")];
    char script_stringpool_str94[sizeof("Ogham")];
    char script_stringpool_str95[sizeof("Malayalam")];
    char script_stringpool_str96[sizeof("Kaithi")];
    char script_stringpool_str97[sizeof("Ethiopic")];
    char script_stringpool_str98[sizeof("Vai")];
    char script_stringpool_str100[sizeof("Psalter_Pahlavi")];
    char script_stringpool_str101[sizeof("Khudawadi")];
    char script_stringpool_str103[sizeof("Imperial_Aramaic")];
    char script_stringpool_str104[sizeof("Pau_Cin_Hau")];
    char script_stringpool_str105[sizeof("Old_Italic")];
    char script_stringpool_str106[sizeof("Phags_Pa")];
    char script_stringpool_str107[sizeof("Egyptian_Hieroglyphs")];
    char script_stringpool_str108[sizeof("Old_South_Arabian")];
    char script_stringpool_str109[sizeof("Katakana")];
    char script_stringpool_str110[sizeof("New_Tai_Lue")];
    char script_stringpool_str111[sizeof("Hangul")];
    char script_stringpool_str112[sizeof("Inscriptional_Pahlavi")];
    char script_stringpool_str113[sizeof("Inscriptional_Parthian")];
    char script_stringpool_str114[sizeof("Old_Permic")];
    char script_stringpool_str115[sizeof("Hiragana")];
    char script_stringpool_str117[sizeof("Armenian")];
    char script_stringpool_str118[sizeof("Mahajani")];
    char script_stringpool_str119[sizeof("Nabataean")];
    char script_stringpool_str120[sizeof("Lepcha")];
    char script_stringpool_str121[sizeof("Arabic")];
    char script_stringpool_str122[sizeof("Palmyrene")];
    char script_stringpool_str123[sizeof("Elbasan")];
    char script_stringpool_str124[sizeof("Runic")];
    char script_stringpool_str125[sizeof("Gujarati")];
    char script_stringpool_str130[sizeof("Tai_Viet")];
    char script_stringpool_str133[sizeof("Old_Turkic")];
    char script_stringpool_str134[sizeof("Avestan")];
    char script_stringpool_str139[sizeof("Ol_Chiki")];
    char script_stringpool_str142[sizeof("Linear_A")];
    char script_stringpool_str143[sizeof("Old_North_Arabian")];
    char script_stringpool_str147[sizeof("Rejang")];
    char script_stringpool_str150[sizeof("Hebrew")];
    char script_stringpool_str164[sizeof("Pahawh_Hmong")];
    char script_stringpool_str165[sizeof("Kayah_Li")];
    char script_stringpool_str171[sizeof("Old_Persian")];
  };
static const struct script_stringpool_t script_stringpool_contents =
  {
    "Yi",
    "Mro",
    "Miao",
    "Cham",
    "Mandaic",
    "Common",
    "Myanmar",
    "Chakma",
    "Mongolian",
    "Bamum",
    "Bengali",
    "Brahmi",
    "Sinhala",
    "Carian",
    "Thai",
    "Sharada",
    "Modi",
    "Thaana",
    "Shavian",
    "Syriac",
    "Cuneiform",
    "Lao",
    "Devanagari",
    "Braille",
    "Mende_Kikakui",
    "Samaritan",
    "Cherokee",
    "Meetei_Mayek",
    "Tamil",
    "Buginese",
    "Warang_Citi",
    "Javanese",
    "Balinese",
    "Tagbanwa",
    "Latin",
    "Canadian_Aboriginal",
    "Linear_B",
    "Telugu",
    "Tagalog",
    "Tai_Tham",
    "Nko",
    "Caucasian_Albanian",
    "Phoenician",
    "Tirhuta",
    "Tai_Le",
    "Limbu",
    "Lydian",
    "Saurashtra",
    "Georgian",
    "Grantha",
    "Kannada",
    "Gothic",
    "Osmanya",
    "Batak",
    "Glagolitic",
    "Khmer",
    "Takri",
    "Oriya",
    "Manichaean",
    "Buhid",
    "Coptic",
    "Cypriot",
    "Sora_Sompeng",
    "Siddham",
    "Bopomofo",
    "Duployan",
    "Kharoshthi",
    "Inherited",
    "Meroitic_Cursive",
    "Cyrillic",
    "Lycian",
    "Gurmukhi",
    "Meroitic_Hieroglyphs",
    "Sundanese",
    "Bassa_Vah",
    "Tibetan",
    "Deseret",
    "Tifinagh",
    "Han",
    "Lisu",
    "Greek",
    "Ugaritic",
    "Syloti_Nagri",
    "Hanunoo",
    "Khojki",
    "Ogham",
    "Malayalam",
    "Kaithi",
    "Ethiopic",
    "Vai",
    "Psalter_Pahlavi",
    "Khudawadi",
    "Imperial_Aramaic",
    "Pau_Cin_Hau",
    "Old_Italic",
    "Phags_Pa",
    "Egyptian_Hieroglyphs",
    "Old_South_Arabian",
    "Katakana",
    "New_Tai_Lue",
    "Hangul",
    "Inscriptional_Pahlavi",
    "Inscriptional_Parthian",
    "Old_Permic",
    "Hiragana",
    "Armenian",
    "Mahajani",
    "Nabataean",
    "Lepcha",
    "Arabic",
    "Palmyrene",
    "Elbasan",
    "Runic",
    "Gujarati",
    "Tai_Viet",
    "Old_Turkic",
    "Avestan",
    "Ol_Chiki",
    "Linear_A",
    "Old_North_Arabian",
    "Rejang",
    "Hebrew",
    "Pahawh_Hmong",
    "Kayah_Li",
    "Old_Persian"
  };
#define script_stringpool ((const char *) &script_stringpool_contents)

static const struct named_script script_names[] =
  {
    {-1}, {-1}, {-1},
#line 51 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str3, 36},
    {-1}, {-1},
#line 129 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str6, 114},
    {-1}, {-1},
#line 113 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str9, 98},
    {-1},
#line 91 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str11, 76},
    {-1},
#line 109 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str13, 94},
#line 15 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str14, 0},
#line 37 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str15, 22},
#line 110 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str16, 95},
#line 46 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str17, 31},
#line 98 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str18, 83},
#line 25 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str19, 10},
#line 108 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str20, 93},
#line 33 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str21, 18},
#line 89 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str22, 74},
#line 34 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str23, 19},
#line 114 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str24, 99},
#line 128 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str25, 113},
#line 23 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str26, 8},
#line 64 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str27, 49},
#line 22 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str28, 7},
#line 77 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str29, 62},
#line 35 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str30, 20},
#line 24 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str31, 9},
#line 67 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str32, 52},
#line 127 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str33, 112},
#line 96 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str34, 81},
#line 41 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str35, 26},
#line 100 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str36, 85},
#line 29 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str37, 14},
#line 68 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str38, 53},
#line 139 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str39, 124},
#line 99 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str40, 84},
#line 76 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str41, 61},
#line 59 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str42, 44},
#line 16 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str43, 1},
#line 42 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str44, 27},
#line 62 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str45, 47},
#line 30 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str46, 15},
#line 56 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str47, 41},
#line 92 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str48, 77},
#line 80 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str49, 65},
#line 117 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str50, 102},
#line 78 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str51, 63},
#line 138 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str52, 123},
#line 61 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str53, 46},
#line 60 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str54, 45},
#line 90 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str55, 75},
#line 85 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str56, 70},
#line 38 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str57, 23},
#line 121 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str58, 106},
#line 31 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str59, 16},
#line 53 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str60, 38},
#line 65 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str61, 50},
#line 107 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str62, 92},
#line 71 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str63, 56},
#line 45 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str64, 30},
#line 116 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str65, 101},
#line 28 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str66, 13},
#line 126 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str67, 111},
#line 58 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str68, 43},
#line 69 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str69, 54},
#line 66 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str70, 51},
#line 115 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str71, 100},
#line 136 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str72, 121},
#line 49 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str73, 34},
#line 119 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str74, 104},
#line 75 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str75, 60},
#line 55 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str76, 40},
#line 111 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str77, 96},
#line 18 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str78, 3},
#line 88 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str79, 73},
#line 26 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str80, 11},
#line 112 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str81, 97},
#line 81 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str82, 66},
#line 118 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str83, 103},
#line 36 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str84, 21},
#line 54 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str85, 39},
#line 72 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str86, 57},
#line 50 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str87, 35},
#line 97 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str88, 82},
#line 17 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str89, 2},
#line 63 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str90, 48},
#line 73 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str91, 58},
#line 57 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str92, 42},
#line 123 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str93, 108},
#line 43 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str94, 28},
#line 32 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str95, 17},
#line 106 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str96, 91},
#line 40 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str97, 25},
#line 84 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str98, 69},
    {-1},
#line 135 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str100, 120},
#line 137 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str101, 122},
    {-1},
#line 101 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str103, 86},
#line 133 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str104, 118},
#line 52 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str105, 37},
#line 79 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str106, 64},
#line 95 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str107, 80},
#line 102 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str108, 87},
#line 48 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str109, 33},
#line 70 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str110, 55},
#line 39 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str111, 24},
#line 104 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str112, 89},
#line 103 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str113, 88},
#line 134 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str114, 119},
#line 47 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str115, 32},
    {-1},
#line 19 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str117, 4},
#line 125 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str118, 110},
#line 131 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str119, 116},
#line 82 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str120, 67},
#line 21 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str121, 6},
#line 132 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str122, 117},
#line 120 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str123, 105},
#line 44 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str124, 29},
#line 27 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str125, 12},
    {-1}, {-1}, {-1}, {-1},
#line 93 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str130, 78},
    {-1}, {-1},
#line 105 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str133, 90},
#line 94 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str134, 79},
    {-1}, {-1}, {-1}, {-1},
#line 83 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str139, 68},
    {-1}, {-1},
#line 124 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str142, 109},
#line 130 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str143, 115},
    {-1}, {-1}, {-1},
#line 87 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str147, 72},
    {-1}, {-1},
#line 20 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str150, 5},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1},
#line 122 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str164, 107},
#line 86 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str165, 71},
    {-1}, {-1}, {-1}, {-1}, {-1},
#line 74 "./unictype/scripts_byname.gperf"
    {(int)(long)&((struct script_stringpool_t *)0)->script_stringpool_str171, 59}
  };

#ifdef __GNUC__
__inline
#if defined __GNUC_STDC_INLINE__ || defined __GNUC_GNU_INLINE__
__attribute__ ((__gnu_inline__))
#endif
#endif
const struct named_script *
uc_script_lookup (register const char *str, register unsigned int len)
{
  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register int key = scripts_hash (str, len);

      if (key <= MAX_HASH_VALUE && key >= 0)
        {
          register int o = script_names[key].name;
          if (o >= 0)
            {
              register const char *s = o + script_stringpool;

              if (*str == *s && !strcmp (str + 1, s + 1))
                return &script_names[key];
            }
        }
    }
  return 0;
}
