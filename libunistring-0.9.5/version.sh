# Version number and release date.
VERSION_NUMBER=0.9.5
RELEASE_DATE=2015-02-16      # in "date +%Y-%m-%d" format

# Version of gnulib that was used in this release.
GNULIB_GIT_COMMIT=784023c966667c8014acd4c3296f7da7418224f8
