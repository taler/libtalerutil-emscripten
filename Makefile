.PHONY: all
all: wrapper

.PHONY: debug
debug: debug-wrapper

.PHONY: gnunet
gnunet: libgcrypt libunistring submodules
	./build-gnunet

.PHONY: exchange
exchange: gnunet submodules
	./build-exchange

.PHONY: libgcrypt
libgcrypt: libgpg-error
	./build-libgcrypt

.PHONY: libgpg-error
libgpg-error: submodules
	./build-libgpg-error

.PHONY: libunistring submodules
libunistring: submodules
	./build-libunistring

.PHONY: wrapper
wrapper: exchange
	./build-wrapper

.PHONY: debug-wrapper
debug-wrapper: exchange
	./build-debug-wrapper

.PHONY: submodules
submodules:
	git submodule update --init --force
