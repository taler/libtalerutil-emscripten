#!/bin/sh
#  This file is part of TALER
#  Copyright (C) 2016 INRIA
#
#  TALER is free software; you can redistribute it and/or modify it under the
#  terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 3, or (at your option) any later version.
#
#  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
#  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with
#  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

# NOTE: it has *no* effect passing optimizing flags for linking into JavaScript (such as
# -O2, -O3) to libraries we link against, since those will serve us in the form of bitcode.
#

set -eu

SCRIPT=$(readlink -f "$0")
DIR=$(dirname "$SCRIPT")
OUT=$DIR/out

exports=$(cat exports.txt)

emcc -c -v -O3 -Wall -I$OUT/emscripten/include \
  wrap.c

emcc -v -O3 --memory-init-file 0 -Wall \
  -s NO_DYNAMIC_EXECUTION=1 \
  -s ASSERTIONS=1 \
  -s MODULARIZE=1 \
  -s EXPORT_NAME="'TalerEmscriptenLib'" \
  -s EXPORTED_FUNCTIONS="$exports" \
  -s BINARYEN_METHOD=native-wasm \
  -s WASM=1 \
  -s EXTRA_EXPORTED_RUNTIME_METHODS='["ccall", "cwrap", "stringToUTF8", "getValue", "setValue", "Pointer_stringify"]' \
  -s FS_LOG=1 \
  wrap.o \
  $OUT/emscripten/lib/libgnunetutil_taler_wallet.a \
  $OUT/emscripten/lib/libgcrypt.a \
  $OUT/emscripten/lib/libgpg-error.a \
  $OUT/emscripten/lib/libunistring.a \
  $OUT/emscripten/lib/libtalerutil_wallet.a \
  -o taler-emscripten-lib.js
